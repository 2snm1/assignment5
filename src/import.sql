SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `test_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `test_db` ;

-- -----------------------------------------------------
-- Table `test_db`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test_db`.`product` (
  `product_id` INT(11) NOT NULL,
  `product_name` VARCHAR(40) NULL,
  `product_price` DOUBLE(11,4) NOT NULL,
  `product_quantity` INT(11) NOT NULL DEFAULT 0,
  `product_reorder_level` INT(11) NOT NULL DEFAULT 0,
  `product_status` INT(1) NULL,
  PRIMARY KEY (`product_id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
