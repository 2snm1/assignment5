package com.msc.iss.product;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import jdk.nashorn.internal.ir.annotations.Ignore;

import org.glassfish.jersey.server.mvc.Template;
import org.glassfish.jersey.server.mvc.Viewable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.msc.iss.dao.ProductDao;
import com.msc.iss.util.Utils;
@Path("/")
@Produces(MediaType.TEXT_HTML)
@Singleton
@Template
@Entity
@Table(name = "product")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Product {
	
	@Id
	@Column(name = "product_id", nullable = false, unique = true, columnDefinition = "INT(11)")
	private int productId = 0;
	@Column(name = "product_name", columnDefinition = "")
	private String productName = null;
	@Column(name = "product_price", columnDefinition = "")
	private BigDecimal productPrice = null;
	@Column(name = "product_quantity", columnDefinition = "")
	private int productQuantity = 0;
	@Column(name = "product_reorder_level", columnDefinition = "")
	private int productReorderLevel = 0;
	@Column(name = "product_status", columnDefinition = "")
	boolean productStatus = true;
@Transient
Map<String, Object> resp =new HashMap<String, Object>();
@JsonIgnore
	public Map<String, Object> getResp() {
	return resp;
}

	public Product() {

	}

	public Product(int productId, String productName, BigDecimal productPrice, int productQuantity,
			int productReorderLevel, boolean productStatus) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productPrice = productPrice;
		this.productQuantity = productQuantity;
		this.productReorderLevel = productReorderLevel;
		this.productStatus = productStatus;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		this.productPrice = productPrice;
	}

	public int getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}

	public int getProductReorderLevel() {
		return productReorderLevel;
	}

	public void setProductReorderLevel(int productReorderLevel) {
		this.productReorderLevel = productReorderLevel;
	}

	public boolean isProductStatus() {
		return productStatus;
	}

	public void setProductStatus(boolean productStatus) {
		this.productStatus = productStatus;
	}


	@JsonIgnore
	@GET
//	@Produces({MediaType.APPLICATION_XML, MediaType.TEXT_XML, MediaType.APPLICATION_JSON})
	public Viewable getProducts(){
		List<Product> res=ProductDao.list();
		/*if(res==null){
			 throw new NotFoundException(Response
	                    .status(Response.Status.NOT_FOUND)
	                    .entity("There are no items in the database")
	                    .build());
		}*/
			getResp().put("entity", res);
			if(res==null){
				return new Viewable("/newProduct");
			}else{
				return new Viewable("/index",this);
			}
	}
	@GET
	@Path("product/{productId}")
	public Viewable getItem(@PathParam("productId") Integer productId) {
        Product response=null;
		try {
			response = ProductDao.findById(productId);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		    throw new NotFoundException(Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("An error occured")
                    .build());
		}
        if (response == null) {
            throw new NotFoundException(Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("That product does not exist")
                    .build());
        }

        return new Viewable("/product", response);
    }

	@Path("product/delete/{productId}")
	@POST
	public Viewable deleteItem(@PathParam("productId") Integer productId) {
		if(!ProductDao.delete(productId)){
		return new Viewable("/error");
		}else{
			return getProducts();
		}
		
	}
	

	
	@Path("product")
	@POST
	public Viewable addItem(@FormParam("productName") String productName,
			@FormParam("productStatus") boolean productStatus,
			@FormParam("productPrice") BigDecimal productPrice,
			@FormParam("productId") int productId,
			@FormParam("productQuantity") int productQuantity,
			@FormParam("productReorderLevel") int productReorderLevel){
		Integer i=ProductDao.add(new Product(productId, productName, productPrice, productQuantity, productReorderLevel,productStatus ));
		if(i==null || i.intValue()<0){
			return new Viewable("/error");
		}
		
		return getProducts();
	}
	@Path("product/{productId}")
	@POST
	public Viewable putItem(@FormParam("productName") String productName,
			@FormParam("productStatus") boolean productStatus,
			@FormParam("productPrice") BigDecimal productPrice,
			@PathParam("productId") int productId,
			@FormParam("productQuantity") int productQuantity,
			@FormParam("productReorderLevel") int productReorderLevel){
		
		
		if(!ProductDao.update(new Product(productId, productName, productPrice, productQuantity, productReorderLevel, productStatus))){
			return new Viewable("/error");
		}else{
			return getProducts();
		}
	}
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice
				+ ", productQuantity=" + productQuantity + ", productReorderLevel=" + productReorderLevel
				+ ", productStatus=" + productStatus + ", resp=" + resp + "]";
	}



}
