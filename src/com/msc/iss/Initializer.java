package com.msc.iss;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.SessionFactory;

import com.msc.iss.util.HibernateUtil;
import com.msc.iss.util.Utils;




public class Initializer implements ServletContextListener{
	private static SessionFactory factory = null;
	private Utils utils=new Utils();
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		if(factory!=null){
			factory.close();
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		utils.createDB();
		factory = HibernateUtil.getSessionJavaConfigFactory("jdbc:mysql://localhost:3306/test_db");
		System.out.println("done");
		
	}

}
