package com.msc.iss.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.msc.iss.product.Product;
import com.msc.iss.util.HibernateUtil;

public class ProductDao {

	private static SessionFactory factory = HibernateUtil
			.getSessionJavaConfigFactory("jdbc:mysql://localhost:3306/test_db");

	protected static Session getSession() {
		return factory.getCurrentSession();

	}

	public static int add(Product object) {
		Session session = getSession();
		Transaction tx = null;
		Integer id = null;
		try {
			tx = session.beginTransaction();
			id = (Integer) session.save(object);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return -1;

		}
		if (id == null) {

			return -1;
		} else {
			return id;
		}
	}

	public static boolean update(Product object) {
		Session session = getSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(object);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return false;

		}
		return true;
	}

	public static boolean delete(int id) {
		Session session = getSession();
		Transaction tx = null;
		Product object = null;
		try {
			tx = session.beginTransaction();
			object = (Product) session.get(Product.class, id);
			if (object == null) {
				return false;
			}
			session.delete(object);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return false;
		}
		return true;

	}

	@SuppressWarnings("unchecked")
	public static List<Product> list() {
		List<Product> objects = null;
		Session session = getSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria cr = getSession().createCriteria(Product.class);
			cr.addOrder(Order.asc("productId"));
			cr.setFirstResult(0);

			objects = (List<Product>) cr.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		}
		System.out.println(objects);
		if (objects == null || objects.isEmpty()) {

			return null;
		} else {
			return objects;
		}
	}

	public static Product findById(int id) throws JsonProcessingException {

		Product object = null;
		Session session = getSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			object = (Product) session.get(Product.class, id);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		}
		if (object == null) {
			return null;
		} else {

			return object;
		}
	}
}
