package com.msc.iss;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;





//@ApplicationPath("")
public class Assignment extends ResourceConfig {
	public Assignment() {
		packages("com.msc.iss").register(JspMvcFeature.class).register(LoggingFilter.class);
	}
}
