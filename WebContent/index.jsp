<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%-- <%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Products</title>
</head>
<body>
<h1>Products List</h1>
<c:forEach var="product" items="${model.resp.entity}">
<li><a href="product/${product.productId}/">${product.productName}</a><br/>
 </c:forEach>
 <br/>
 ----------------------------------<br/>
 <h2>New Product</h2>
 <FORM METHOD=POST ACTION="/Assignment5/product">
Product name <INPUT TYPE=TEXT NAME=productName SIZE=50><BR>
Product Id: <INPUT TYPE=TEXT NAME=productId SIZE=50><BR>
Product price <INPUT TYPE=number NAME=productPrice SIZE=20><BR>
Product quantity: <INPUT TYPE=number NAME=productQuantity SIZE=4>
Product status: <INPUT TYPE=checkbox NAME=productStatus checked><BR>
Product reorder level: <INPUT TYPE=TEXT NAME=productReorderLevel SIZE=50><BR>
<P><INPUT TYPE=SUBMIT VALUE="Submit">
</FORM>
 
</body>
</html>