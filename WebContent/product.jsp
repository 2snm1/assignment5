<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="rbt" uri="urn:org:glassfish:jersey:servlet:mvc" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Product</title>
    </head>
    <body>

        <h1>${model.productName}</h1>

        Id: ${model.productId}<br/>
        Quantity: ${model.productQuantity}<br/>
        ReorderLevel: ${model.productReorderLevel}<br/>
        Status: ${model.productStatus}<br/>
        Price: ${model.productPrice}<br/>
       <br/>------------------------------------------<br/> 
       <form METHOD=POST action="/Assignment5/product/delete/${model.productId}">
    <input type="submit" value="Delete">
</form>      
       <br/>------------------------------------------<br/> 
<FORM METHOD=POST ACTION="/Assignment5/product/${model.productId}">
Product name <INPUT TYPE=TEXT NAME=productName SIZE=50 VALUE="${model.productName}"><BR>
Product price <INPUT TYPE=number NAME=productPrice SIZE=20 VALUE="${model.productPrice}"><BR>
Product quantity: <INPUT TYPE=number NAME=productQuantity SIZE=4 VALUE="${model.productQuantity}">
Product status: <INPUT TYPE=checkbox NAME=productStatus VALUE="${model.productStatus}"><BR>
Product reorder level: <INPUT TYPE=TEXT NAME=productReorderLevel SIZE=50 VALUE="${model.productReorderLevel}"><BR>
<P><INPUT TYPE=SUBMIT VALUE="Submit">
</FORM>
        <jsp:include page="footer.jsp"/>

    </body>
</html>